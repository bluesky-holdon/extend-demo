package com.lubase.extenddemo.sso.service;


import com.lubase.orm.model.LoginUser;

public interface SSOUserService {
    LoginUser getSSOUser(String token);
}
