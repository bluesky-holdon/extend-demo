package com.lubase.extenddemo.sso.controller;

import com.lubase.core.config.PassToken;
import com.lubase.core.response.ResponseData;
import com.lubase.orm.model.LoginUser;
import com.lubase.extenddemo.sso.service.Impl.SSOUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * sso用户登录、密码服务等
 */
@PassToken
@RestController
@RequestMapping("ssouser")
public class SSOUserController {

    @Autowired
    SSOUserServiceImpl ssoUserService;


    @PassToken
    @PostMapping(value = "ssologin")
    public ResponseData<LoginUser> login(@RequestParam String ssoToken) {
        if (StringUtils.isEmpty(ssoToken)) {
            return ResponseData.parameterNotFound("ssoToken");
        }
        //pwd 存放sso_token
        var user = ssoUserService.getSSOUser(ssoToken);
        if (user == null) {
            return ResponseData.error("401", "用户名或密码错误");
        } else {
            return ResponseData.success(user);
        }
    }
}
