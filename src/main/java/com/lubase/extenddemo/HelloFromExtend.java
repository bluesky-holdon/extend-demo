package com.lubase.extenddemo;

import com.lubase.core.extend.IInvokeMethod;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class HelloFromExtend implements IInvokeMethod {
    @Override
    public boolean checkRight() {
        return false;
    }

    @Override
    public Object exe(HashMap<String, String> hashMap) throws Exception {
        return "hello lubase";
    }

    @Override
    public String getDescription() {
        return "测试扩展方法：hello lubase";
    }

    @Override
    public String getId() {
        // 设置唯一的方法id
        return "1181294501122019328";
    }
}
